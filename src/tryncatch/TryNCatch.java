/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tryncatch;

import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class TryNCatch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        printMenu();
        switch(input.next().charAt(0)){
            case '1':
                System.out.println("Ahora el programa esperará un número entero"
                        + " introduce letras o decimales para obtener una excep"
                        + "cion lanzada por la clase scanner y averigua como se"
                        + " llama dicha excepcion");
                input.nextInt();
            break;
            case '2':
                System.out.println("Vamos a crear un array de 3 posiciones y es"
                        + "cribir 4 datos en el para que el compilador nos de u"
                        + "na excepcion de limites del array");
                int [] listaEnteros = new int [3];
                for (int i = 0; i<listaEnteros.length+1; i++){
                    System.out.println("Introduciendo el número "+(i+1)+" en la"
                            + "posicion "+i);
                    listaEnteros[i] = i+1;
                }
                break;
            case '3':
                System.out.println("Vamos a ocasionar que una clase creada por "
                        + "nosotros arroje una excepción que tambien hemos crea"
                        + "do nosotros");
                System.out.println("Vamos a crear una persona con una edad real"
                        + "y luego otra con una edad negativa, lo que dará como"
                        + " resultado un error");
            break;
            case '4':
                System.out.println("Bye!");
                break;
            default:
                System.out.println("Elige una opcion valida");
                
        }
     
        
    }
    
    static void printMenu(){
        System.out.println("Elige una opción: ");
        System.out.println("1) Causar un error de lectura de datos");
        System.out.println("2) Causar un error de limites de array");
        System.out.println("3) Causar un error personalizado");
        System.out.println("4) Salir");
    }
    
}
