/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tryncatch;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Persona {
    
    private int edad;
    private String nombre;
    
    public Persona (String nombre, int edad) throws EdadMustBePositiveException{
        if (edad<0){
            throw new EdadMustBePositiveException();
        }else{
            this.edad = edad;
        }
    }
    
    @Override
    public String toString(){
        return "Esta persona se llama "+this.nombre+" y tiene "+this.edad+" años";
    }
}
